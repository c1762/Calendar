package com.example.calender;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CalendarViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public final TextView daysOfMonth;
    private final CalendarAdapter.OnItemListner onItemListner;

    public CalendarViewHolder(@NonNull View itemView, CalendarAdapter.OnItemListner onItemListner) {
        super(itemView);
        daysOfMonth = itemView.findViewById(R.id.cellDayText);
        this.onItemListner = onItemListner;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        onItemListner.onItemClick(getAdapterPosition(), (String) daysOfMonth.getText());
    }
}
