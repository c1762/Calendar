package com.example.calender;

import android.view.ViewGroup;
import android.view.View;
import android.view.LayoutInflater;
import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

class CalendarAdapter extends RecyclerView.Adapter<CalendarViewHolder> {

    private final ArrayList<String> daysOfMonth;
    private final OnItemListner onItemListner;

    public CalendarAdapter(ArrayList<String> daysOfMonth, OnItemListner onItemListner) {
        this.daysOfMonth = daysOfMonth;
        this.onItemListner = onItemListner;
    }

    @NonNull
    @Override
    public CalendarViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.calendar_cell, parent, false);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = (int)(parent.getHeight() * 0.166666666 );
        return new CalendarViewHolder(view, onItemListner);
    }

    @Override
    public void onBindViewHolder(@NonNull CalendarViewHolder holder, int position) {
        holder.daysOfMonth.setText(daysOfMonth.get(position));


    }

    @Override
    public int getItemCount() {
        return daysOfMonth.size();
    }

    public interface OnItemListner{
        void onItemClick(int position, String dayText);
    }

}
